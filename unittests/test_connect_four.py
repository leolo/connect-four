import unittest
from Connect_Four.connect_four import Connect_Four

class Test_Connect_Four(unittest.TestCase):
    def setUp(self):
        self.connect_four = Connect_Four()

    def test_randomize_starting_player(self):
        connect_four = Connect_Four()
        self.assertIn(connect_four.player_1.player_no, [1, 2])