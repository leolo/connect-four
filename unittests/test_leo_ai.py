import unittest
from Player.leo_ai import Leo_AI
from Playing_Field.playing_field import Playing_Field

class Test_Leo_AI(unittest.TestCase):
    def setUp(self):
        self.leo_ai = Leo_AI()
        self.leo_ai.player_no = 1

    def test_turn_first_round(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.turn(0, playing_field)
        self.assertEqual(move, 3)

    def test_turn_win_this_turn(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 1, 2, 1, 0],
            [1, 0, 1, 1, 2, 2, 0],
            [1, 0, 1, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        self.assertIn(self.leo_ai.turn(14, playing_field), [1, 2, 3])

    def test_turn_prevent_opponent_win(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 2, 0, 0],
            [0, 0, 1, 1, 2, 1, 0],
            [0, 0, 1, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.turn(9, playing_field)
        self.assertEqual(move, 4)
    
    def test_turn_prevent_opponent_win_2(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0],
            [0, 0, 2, 2, 2, 0, 0],
            [0, 0, 2, 1, 2, 0, 0],
            [0, 1, 1, 2, 1, 0, 0],
            [0, 2, 1, 1, 1, 2, 0]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.turn(9, playing_field)
        self.assertNotEqual(move, 1)

    def test_turn_prevent_opponent_win_3(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 2, 2, 2, 0, 0],
            [0, 0, 1, 1, 1, 2, 0],
            [1, 0, 2, 1, 1, 1, 2]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.turn(9, playing_field)
        self.assertEqual(move, 5)

    def test_turn_prevent_quandary(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 1],
            [0, 0, 0, 0, 0, 2, 2],
            [0, 0, 1, 2, 2, 1, 1],
            [1, 1, 2, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.turn(11, playing_field)
        self.assertEqual(move, 4)

    def test_turn_prevent_get_out_of_quandary_2(self):      
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 2, 0, 0],
            [0, 0, 0, 2, 1, 0, 0],
            [0, 0, 2, 1, 1, 2, 0],
            [0, 1, 2, 1, 1, 2, 0]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.turn(11, playing_field)
        self.assertEqual(move, 5)

    def test_turn_prevent_get_out_of_quandary_3(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 2, 2, 0, 0],
            [0, 0, 1, 2, 1, 1, 2],
            [0, 0, 2, 1, 2, 2, 1],
            [0, 1, 2, 1, 2, 1, 1]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.turn(11, playing_field)
        self.assertEqual(move, 2)

    def test_turn_prevent_get_out_of_field(self):  
        data = [
            [0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 2, 0],
            [0, 0, 0, 0, 1, 1, 0],
            [0, 0, 2, 2, 2, 1, 0],
            [0, 2, 2, 1, 1, 1, 2]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.turn(11, playing_field)
        self.assertEqual(move, 1)

    def test_turn_prevent_get_out_of_field_2(self):
        data = [
            [0, 1, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 0],
            [0, 2, 0, 0, 0, 0, 0],
            [0, 1, 1, 0, 0, 0, 0],
            [0, 1, 2, 2, 2, 0, 0],
            [2, 1, 1, 1, 2, 2, 0]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.turn(11, playing_field)
        self.assertEqual(move, 5)

    def test_predict_next_rounds(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 1, 2, 1, 0],
            [1, 0, 1, 1, 2, 2, 0],
            [1, 0, 1, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        self.leo_ai.predict_next_rounds(playing_field, [0, 1, 2, 4])
        self.assertGreater(len(self.leo_ai.losing_scenarios), 1)

    def test_predict_next_rounds_start(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 2, 1, 0, 0, 0],
            [0, 0, 2, 1, 0, 0, 0],
            [0, 0, 2, 1, 0, 0, 0]
        ]
        playing_field = Playing_Field(data=data)
        self.leo_ai.predict_next_rounds(playing_field, [0, 1, 2, 4, 6])
        self.assertGreater(len(self.leo_ai.losing_scenarios), 1)

    def test_make_move(self):
        expected_result = [
            [0, 0, 0, 0, 0, 0, 0],
            [1, 0, 1, 1, 2, 1, 0],
            [1, 0, 1, 1, 2, 2, 0],
            [1, 0, 1, 1, 2, 2, 2]
        ]
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 1, 2, 1, 0],
            [1, 0, 1, 1, 2, 2, 0],
            [1, 0, 1, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        playing_field, prediction = self.leo_ai.make_move(
            playing_field,
            0,
            1,
            []
        )
        self.assertEqual(playing_field.data, expected_result)

    def test_make_move_no_valid_move_for_column(self):
        data = [
            [1, 0, 0, 0, 0, 0, 0],
            [1, 0, 1, 1, 2, 1, 0],
            [1, 0, 1, 1, 2, 2, 0],
            [1, 0, 1, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        playing_field, prediction = self.leo_ai.make_move(
            playing_field,
            0,
            1,
            []
        )
        self.assertEqual(prediction, [])

    def test_last_breath(self):
        data = [
            [0, 1, 1, 2, 1, 2, 2],
            [0, 1, 1, 2, 2, 2, 1],
            [0, 2, 2, 2, 1, 1, 2],
            [0, 2, 1, 1, 2, 2, 1],
            [1, 1, 2, 2, 1, 2, 2],
            [1, 1, 2, 1, 1, 2, 1]
        ]
        playing_field = Playing_Field(data=data)
        move = self.leo_ai.last_breath(
            playing_field
        )
        self.assertEqual(move, 0)