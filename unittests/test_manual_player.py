import unittest
from Player.manual_player import Manual_Player

class Test_Manual_Player(unittest.TestCase):
    def setUp(self):
        self.manual_player = Manual_Player()

    def test_check_if_input_is_valid_higher(self):
        self.assertEqual(self.manual_player.check_if_input_is_valid(7), None)

    def test_check_if_input_is_valid_lower(self):
        self.assertEqual(self.manual_player.check_if_input_is_valid(-1), None)

    def test_check_if_input_is_valid_lowest(self):
        self.assertTrue(self.manual_player.check_if_input_is_valid(0))

    def test_check_if_input_is_valid_highest(self):
        self.assertTrue(self.manual_player.check_if_input_is_valid(6))