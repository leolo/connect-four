import unittest
from Playing_Field.playing_field import Playing_Field

class Test_Playing_Field(unittest.TestCase):
    def setUp(self):
        self.playing_field = Playing_Field()

    def test_create_field(self):
        playing_field = Playing_Field(7, 6)
        self.assertEqual(len(playing_field.data), 6)
        self.assertEqual(len(playing_field.data[0]), 7)

    def test_print_playing_field(self):
        playing_field = Playing_Field(7, 6)
        print(str(playing_field))

    def test_make_move(self):
        data = [
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 0]
        ]
        expected_result = [
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 2]
        ]
        playing_field = Playing_Field(data=data)
        playing_field.make_move(2, 6)
        self.assertEqual(playing_field.data, expected_result)

    def test_make_move_no_free_field_in_column(self):
        data = [
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 0]
        ]
        playing_field = Playing_Field(data=data)
        with self.assertRaises(Exception):
            playing_field.make_move(2, 3)

    def test_check_horizontal(self):
        data = [
            [0, 0, 0, 1, 1, 1, 1]
        ]
        playing_field = Playing_Field(data=data)
        self.assertTrue(playing_field.game_over())

    def test_check_horizontal_false(self):
        data = [
            [0, 0, 0, 0, 1, 1, 1]
        ]
        playing_field = Playing_Field(data=data)
        self.assertFalse(playing_field.game_over())

    def test_check_vertical(self):
        data = [
            [0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 0]
        ]
        playing_field = Playing_Field(data=data)
        self.assertTrue(playing_field.game_over())

    def test_check_vertical_false(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 0],
            [0, 0, 0, 1, 2, 0, 0]
        ]
        playing_field = Playing_Field(data=data)
        self.assertFalse(playing_field.game_over())

    def test_check_diagonal_ascending(self):
        data = [
            [0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 1, 2, 0],
            [0, 0, 1, 1, 2, 2, 0],
            [0, 0, 1, 1, 2, 2, 0]
        ]
        playing_field = Playing_Field(data=data)
        self.assertTrue(playing_field.game_over())

    def test_check_diagonal_ascending_false(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 1, 2, 0],
            [0, 0, 1, 1, 2, 2, 0],
            [0, 0, 1, 1, 2, 2, 0]
        ]
        playing_field = Playing_Field(data=data)
        self.assertFalse(playing_field.game_over())

    def test_check_diagonal_descending(self):
        data = [
            [0, 0, 0, 2, 0, 0, 0],
            [0, 0, 1, 1, 2, 1, 0],
            [0, 0, 1, 1, 2, 2, 0],
            [0, 0, 1, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        self.assertTrue(playing_field.game_over())

    def test_check_diagonal_descending_false(self):
        data = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 1, 2, 1, 0],
            [0, 0, 1, 1, 2, 2, 0],
            [0, 0, 1, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        self.assertFalse(playing_field.game_over())

    def test_draw(self):
        data = [
            [1, 1, 2, 2, 1, 2, 1],
            [2, 2, 1, 1, 2, 1, 1],
            [1, 2, 1, 1, 2, 2, 1],
            [1, 2, 1, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        self.assertTrue(playing_field.draw())

    def test_draw_false(self):
        data = [
            [0, 1, 2, 2, 1, 2, 1],
            [2, 2, 1, 1, 2, 1, 1],
            [1, 2, 1, 1, 2, 2, 1],
            [1, 2, 1, 1, 2, 2, 2]
        ]
        playing_field = Playing_Field(data=data)
        self.assertEqual(playing_field.draw(), None)