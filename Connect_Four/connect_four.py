import random
from Playing_Field.playing_field import Playing_Field
from Player.player import Player
from Player.leo_ai import Leo_AI
from Player.manual_player import Manual_Player

class Connect_Four:
    def __init__(self):
        self.move_no = 0
        self.playing_field = Playing_Field()
        self.player_1, self.player_2 = None, None
        player_inst_1 = Leo_AI()
        player_inst_2 = Manual_Player()
        self.__randomize_starting_player(player_inst_1, player_inst_2)

    def __randomize_starting_player(self, player_inst_1, player_inst_2):
        rand_int = random.randint(1, 2)
        if rand_int == 1:
            self.__set_player(player_inst_1, player_inst_2)
        else:
            self.__set_player(player_inst_2, player_inst_1)
    
    def __set_player(self, player_1: Player, player_2: Player):
        self.player_1 = player_1
        self.player_1.player_no = 1
        self.player_2 = player_2
        self.player_2.player_no = 2

    def play(self):
        """Runs a connect four game."""
        self.__process_game()
        self.__finish_game()

    def reset_game(self):
        """Resets game, to be able to play again."""
        self.playing_field = Playing_Field()
        self.move_no = 0

    def __process_game(self):
        while not self.playing_field.game_over():
            print(self.playing_field)
            if self.move_no % 2 == 0:
                self.__play_round(self.player_1)
            else:
                self.__play_round(self.player_2)
            self.move_no += 1
    
    def __play_round(self, player: Player):
        move_no_on_field = player.turn(self.move_no, self.playing_field)
        self.playing_field.make_move(player.player_no, move_no_on_field)

    def __finish_game(self):
        game_over, game_over_type = self.playing_field.game_over()
        print(self.playing_field)
        if game_over_type == 'win':
            if self.move_no % 2 == 0:
                self.__print_winner(self.player_2)
            else:
                self.__print_winner(self.player_1)
            self.__scoreboard()
        else:
            print(f'DRRRRRRRRRAW!')

    def __print_winner(self, player: Player):
        print(f'Player {player.player_no}: {type(player).__name__} won!')
        player.wins += 1

    def __scoreboard(self):
        print(f'\n{type(self.player_1).__name__} wins: {self.player_1.wins}')
        print(f'{type(self.player_2).__name__} wins: {self.player_2.wins}')