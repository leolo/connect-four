import copy
from abc import ABC, abstractmethod
from Playing_Field.playing_field import Playing_Field

class Player(ABC):
    def __init__(self):
        self.player_no = 0
        self.wins = 0

    @abstractmethod
    def turn(self, move_no: int, playing_field: Playing_Field):
        raise NotImplementedError