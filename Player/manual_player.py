from Player.player import Player
from Playing_Field.playing_field import Playing_Field

class Manual_Player(Player):
    def turn(self, move_no: int, playing_field: Playing_Field):
        player_interaction = (
            f'''Hey player {self.player_no} you are dranny bunny!
            \rEnter a number between 0-6 to make your move: '''
        )
        while True:
            move = int(input(player_interaction))
            if self.check_if_input_is_valid(move):
                return move

    def check_if_input_is_valid(self, move):
        if move in range(0, 7):
            return True
        else:
            print('Input not 0-6. Please try again.')