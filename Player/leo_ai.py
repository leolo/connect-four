import random
import copy
from Player.player import Player
from Playing_Field.playing_field import Playing_Field

class Leo_AI(Player):
    def __init__(self):
        super().__init__()
        self.losing_scenarios = []
        self.weighted_predictions = {}
        self.init_dict()

    def turn(self, move_no: int, playing_field: Playing_Field):
        self.reset_predictions()
        self.init_dict()
        self.predict_next_rounds(playing_field, [0, 2, 4, 6])
        self.predict_next_rounds(playing_field, [0, 1, 3])
        return self.process_predictions(playing_field)

    def reset_predictions(self):
        self.losing_scenarios = []
        self.weighted_predictions = {}

    def init_dict(self):
        for i in range(7):
            self.weighted_predictions.setdefault(i, {'sum': 0, 'opponent_wins': 0})

    def predict_next_rounds(self, playing_field: Playing_Field, predict_rounds: list, rounds=0, predicition=[]):
        rounds = predict_rounds[rounds]
        no_of_columns = len(playing_field.data[0])
        current_player = self.get_current_player(rounds)
        for column in range(no_of_columns):
            predicition = predicition[:predict_rounds.index(rounds)]
            prediction_playing_field, predicition = self.make_move(
                playing_field,
                column,
                current_player,
                predicition
            )
            self.process_prediction(prediction_playing_field, predict_rounds, predicition, rounds)

    def make_move(self, playing_field: Playing_Field,
            column_no: int, player_no: int, prediciton: list):
        local_copy = copy.deepcopy(playing_field.data)
        row_index = Playing_Field.get_valid_field_in_column(local_copy, column_no)
        if row_index != None:
            local_copy[row_index][column_no] = player_no
            if not prediciton == None:
                prediciton.append(column_no)
            else:
                prediciton = [column_no]
        return Playing_Field(data=local_copy), prediciton
            
    def process_prediction(self, prediction_playing_field: Playing_Field,
            predict_rounds: list, predicition: list, rounds: int):
        if predicition != None:
            if prediction_playing_field.game_over():
                self.evaluate_prediction(predicition, rounds)
            elif rounds < predict_rounds[-1]:
                self.predict_next_rounds(
                    prediction_playing_field, 
                    predict_rounds, predict_rounds.index(rounds) + 1,
                    predicition
                )

    def evaluate_prediction(self, predicition: list, rounds: int):
        current_player = self.get_current_player(rounds)
        if self.player_no == current_player:
            self.weighted_predictions[predicition[0]]['sum'] += 1/7 ** rounds
        else: 
            self.weighted_predictions[predicition[0]]['sum'] -= 1/7 ** rounds
            if len(predicition) >= 2:
                self.weighted_predictions[predicition[1]]['opponent_wins'] += 1/7 ** rounds
            if len(predicition) == 2:
                self.losing_scenarios.append(predicition)

    def get_current_player(self, rounds: int) -> int:
        if self.player_no == 1:
            other_player_no = 2
        else: 
            other_player_no = 1
        if rounds % 2 == 0:
            return self.player_no
        else:
            return other_player_no

    def process_predictions(self, playing_field: Playing_Field):
        highest_prediction, best_column = self.get_highest_evaluated_prediction()
        if highest_prediction < 0:
            losing_next_round, column = self.prevent_opponent_win()
            if column != None:
                return column
            else:
                return self.last_breath(playing_field)
        return best_column

    def get_highest_evaluated_prediction(self):
        highest_evaluation = [-1, None]
        for column, values in self.weighted_predictions.items():
            if values['sum'] != 0 and values['sum'] > highest_evaluation[0]:
                highest_evaluation = [self.weighted_predictions[column]['sum'], column]

        return highest_evaluation

    def prevent_opponent_win(self):
        return self.get_opponents_highest_win_chance()

    def get_opponents_highest_win_chance(self):
        opponent_highest_win_chance = [-1, None]
        for column, values in self.weighted_predictions.items():
            if values['opponent_wins'] != 0 \
                and values['opponent_wins'] > opponent_highest_win_chance[0] \
                and not self.check_if_column_loses_game(column):
                opponent_highest_win_chance = [self.weighted_predictions[column]['opponent_wins'], column]

        return opponent_highest_win_chance
    
    def check_if_column_loses_game(self, column: int):
        for prediction in self.losing_scenarios:
            if column == prediction[0]:
                return True
    
    def last_breath(self, playing_field: Playing_Field):
        no_of_columns = len(playing_field.data[0])
        possible_moves = []
        for i in range(no_of_columns):
            playing_field, prediction = self.make_move(playing_field, i, self.player_no, [])
            if prediction:
                possible_moves.append(prediction[0])

        if possible_moves:
            return random.choice(possible_moves)