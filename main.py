from Connect_Four.connect_four import Connect_Four

def process_game():
    connect_four = Connect_Four()
    while True:
        connect_four.play()
        if not play_again():
            break
        connect_four.reset_game()

def play_again():
    play_again = input('Enter "y" to play again: ').upper()
    if play_again == 'Y':
        return True
 
if __name__ == '__main__':
    process_game()