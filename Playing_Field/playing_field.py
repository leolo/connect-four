class Playing_Field:
    def __init__(self, width=7, height=6, data=None):
        if data:
            self.__data = data
        else:
            self.__data = self.__create_field(width, height)

    def __str__(self):
        return f'''
            {self.__data[0]}\n
            {self.__data[1]}\n
            {self.__data[2]}\n
            {self.__data[3]}\n
            {self.__data[4]}\n
            {self.__data[5]}\n
        '''

    @property
    def data(self):
        return self.__data

    def __create_field(self, width, height):
        return [[0 for x in range(width)] for y in range(height)]

    def make_move(self, player_no: int, column_no: int):
        """Adds a move to the playing field."""
        row_index = self.get_valid_field_in_column(self.__data, column_no)
        if row_index != None:
            self.__data[row_index][column_no] = player_no
        else:
            raise Exception('Column has no free field anymore!')

    @staticmethod
    def get_valid_field_in_column(playing_field, column_no: int) -> int:
        """Gets the first free field in a column and if available returns row of it."""
        no_of_rows = len(playing_field) - 1
        for row_index, row in enumerate(reversed(playing_field)):
            if row[column_no] == 0:
                return no_of_rows - row_index

    def game_over(self) -> bool:
        """Returns True if the game is over."""
        if self.draw():
            return True, 'draw'
        for row_index, row in enumerate(self.__data):
            for column_index, field in enumerate(row):
                if not field == 0:
                    if self.__check_neighbour_fields(row_index, column_index, field):
                        return True, 'win'

    def draw(self) -> bool:
        """Check if the game ended in a draw."""
        no_of_columns = len(self.data[0])
        available_column = []
        for i in range(no_of_columns):
            prediction = self.get_valid_field_in_column(self.data, i)
            if prediction != None:
                available_column.append(prediction)

        if not available_column:
            return True

    def __check_neighbour_fields(self, row_index: int, column_index: int, value: int):
        horizontal = self.__check_for_win(row_index, column_index, value, 0, 1)
        vertical = self.__check_for_win(row_index, column_index, value, 1, 0)
        diagonal_ascending = self.__check_for_win(row_index, column_index, value, 1, -1)
        diagonal_descending = self.__check_for_win(row_index, column_index, value, 1, 1)

        return horizontal or vertical or diagonal_ascending or diagonal_descending

    def __check_for_win(self, row_index: int, column_index: int, value: int, 
            delta_row: int, delta_column: int, connected=0) -> bool:
        if value == self.__data[row_index][column_index]:
            connected += 1
            if connected == 4:
                return True
            if self.__check_if_within_playing_field(row_index + delta_row, 
                column_index + delta_column):
                return self.__check_for_win(row_index + delta_row, 
                    column_index + delta_column, value, delta_row,
                    delta_column, connected)
            else: return False

    def __check_if_within_playing_field(self, row_index: int, column_index: int) -> bool:
        if (0 <= row_index < len(self.__data) and
        0 <= column_index < len(self.__data[row_index])):
            return True