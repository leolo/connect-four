# Connect Four
A small console connect four game against an AI.

## Requirements:
- Git (to follow this guide to install Connect Four)
- Python 3.7

## Install
Install the program from the terminal with:

`git clone https://gitlab.com/leolo/connect-four.git`

## Run
Change in the cloned connect_four directory:

`cd connect_four`

Run the program with:

`python3 main.py`